package in.silentsudo.es.pipelines.tweeturlextractor;
/*
 * Copyright [2020] [Ashish Agre]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.ingest.AbstractProcessor;
import org.elasticsearch.ingest.IngestDocument;
import org.elasticsearch.ingest.Processor;
import org.nibor.autolink.LinkExtractor;
import org.nibor.autolink.LinkSpan;
import org.nibor.autolink.LinkType;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.elasticsearch.ingest.ConfigurationUtils.readStringProperty;

public class TweetUrlExtractorProcessor extends AbstractProcessor {

    public static final String TYPE = "tweet_url_extractor";

    private final String field;
    private final String targetField;
    private final TweetLinkExtractor tweetLinkExtractor;

    public TweetUrlExtractorProcessor(String tag, String description, String field,
                                      String targetField) throws IOException {
        super(tag, description);
        this.field = field;
        this.targetField = targetField;
        tweetLinkExtractor = new TweetLinkExtractorImpl();
    }

    @Override
    public IngestDocument execute(IngestDocument ingestDocument) throws Exception {
        final String content = ingestDocument.getFieldValue(field, String.class);
        final List<CharSequence> links = tweetLinkExtractor.extract(content);
        ingestDocument.setFieldValue(targetField, links);
        return ingestDocument;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    public static final class Factory implements Processor.Factory {

        @Override
        public TweetUrlExtractorProcessor create(Map<String, Processor.Factory> factories, String tag,
                                                 String description, Map<String, Object> config) throws Exception {
            String field = readStringProperty(TYPE, tag, config, "field");
//            String targetField = readStringProperty(TYPE, tag, config, "target_field", "default_field_name");
            String targetField = readStringProperty(TYPE, tag, config, "target_field", "tweet_links");
            String sentimentField = readStringProperty(TYPE, tag, config, "sentiment_field", "sentiment_field");

            return new TweetUrlExtractorProcessor(tag, description, field, targetField);
        }
    }

    interface TweetLinkExtractor {
        List<CharSequence> extract(CharSequence tweet);
    }

    private static class TweetLinkExtractorImpl implements TweetLinkExtractor {
        private static final Logger logger = LogManager.getLogger(TweetLinkExtractorImpl.class.getSimpleName());
        private final LinkExtractor linkExtractor = LinkExtractor.builder()
                .linkTypes(Set.of(LinkType.URL, LinkType.WWW))
                .build();

        @Override
        public List<CharSequence> extract(CharSequence tweet) {
            if (tweet != null && tweet.length() > 0) {
                final Iterable<LinkSpan> linkSpans = linkExtractor.extractLinks(tweet);
                Stream<LinkSpan> linkStream = StreamSupport.stream(linkSpans.spliterator(), false);
                return linkStream.map(link ->
                        tweet.subSequence(link.getBeginIndex(), link.getEndIndex())).collect(Collectors.toUnmodifiableList());
            } else {
                logger.warn("{} has invalid content", tweet);
                return Collections.emptyList();
            }

        }
    }
}
